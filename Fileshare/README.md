Zuerst habe ich ctrl+shift+T auf dem Linux-Pc gedrückt, um das Terminal zu Öffnen (Terminal ist wie cmd in Windows).

Ich habe folgende Commands nacheinander eingegeben (Die in "") (und immer yes eingegeben, und das Passwort, wenn es verlangt wurde):

"sudo apt install samba"

"systemctl status smbd --no-pager -l"

Danach sollte es so aussehen.

![Alt text](./images/image.png)

"sudo systemctl enable --now smbd"

"sudo ufw allow samba"

"sudo usermod -aG sambashare $USER"

"sudo smbpasswd -a $USER"

Da habe ich ein Passwort meiner Wahl eingegeben und wiederholt.

Ich habe einen neuen Ordner erstellt.

![Alt text](image.png)

Ich habe auf den Ordner Rechtsklick gemacht und Eigenschaften angeklickt.

![Alt text](image-1.png)

Ich habe die die Dinge wie auf dem Bild angetickt.

![Alt text](image-2.png)

Nachher habe ich die Taste zum Erstellen geklickt, und die Berechtigungen hinzugefügt. Somit wäre die Installation beendet.

ich bin im Explorer auf dieser Pc gegangen, und dort habe ich eine neue Netzwerkadresse hinzugefügt.

![Alt text](image-3.png)

Zum Testen habe ich einen Ordner auf dem Client im Server erstellt, und der Wurde dann auch Im Server angezeigt. Voller Erfolg.

![Alt text](image-4.png)