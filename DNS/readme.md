### M123 Dns Server

## Zusammenfassung

## Installation Windows Server 2022

Zuerst habe ich den Server auf die Vm installiert, dann habe ich das Betriebssystem installiert. Ich habe einen weiteren Netzwerkadabter hinzugefügt, und ihn mit dem eigenen Netz verbunden. 

## Dns aufsetzen
Zuerst klickte ich oben rechts auf "Manage", dann auf "Add Roles and Features"

![Alt text](./images/image.png)

Dort habe ich bis auf "Server Roles" weiter geklickt. Dort habe ich "Dns Server" angehackt, und wieder alles weiter gemacht und dann installiert.

![Alt text](./images/image-1.png)

Ich habe oben rechts auf "Tools" und dann auf "DNS" geklickt.

![Alt text](./images/image-2.png)

Im DNS Manager habe ich auf den Server Rechtsklick gemacht, und dann auf "New Zone..."

![Alt text](./images/image-3.png)

Dort habe ich eine fiktive Domäne eingetragen.

![Alt text](./images/image-4.png)

Dann habe ich das Gleiche mit Reverse Lookup gemacht.

![Alt text](./images/image-5.png)

Bei dem Zoneordner habe ich auf neuer Host geklickt.

![Alt text](./images/image-6.png)

Was man dort machen kann: den Name eintragen, und die Ip Adresse eintragen. Wenn man jz zbs bei der Domäne Test.Test den Namen Test nimmt, und die Ip Adresse 192.168.245.99, dann komme ich auf diese Ip Adresse, wenn ich test.test.test eingebe

![Alt text](./images/image-7.png)

Wie man hier sehen kann, sind sie eingetragen

![Alt text](./images/image-8.png)

Ich habe beim Server Properties ausgewählt.

![Alt text](./images/image-9.png)

Bei forwarders hatte ich noch nichts eingetragen, deshalb klickte ich auf "edit".

![Alt text](./images/image-10.png)

Dort habe ich die Ip-Adresse des Google-Servers ausgewählt. Was das macht: Wenn ich auf eine Adresse zugreifen will, die bei meinem Dns Server nicht eingetragen ist, dann leitet er mich an den DNS-Server 8.8.8.8 weiter (von Google). 

![Alt text](./images/image-11.png)






Kurzfassung
Basisinstallation VMWare
Basisinstallation Windows-VM
Konfiguration DNS-Server
Testing